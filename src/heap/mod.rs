#[derive(Debug)]
pub struct Priority<T> {
    score: u32,
    elem: T,
}

impl<T> Priority<T> {
    pub fn new(score: u32, elem: T) -> Self {
        Self { score, elem }
    }
    pub fn get_score(&self) -> u32 {
        self.score
    }
    pub fn get_elem(self) -> T {
        self.elem
    }
}

impl<T> PartialOrd for Priority<T> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.score.partial_cmp(&other.score)
    }
}
impl<T> PartialEq for Priority<T> {
    fn eq(&self, other: &Self) -> bool {
        self.score == other.score
    }
}
impl<T> Eq for Priority<T> {}

impl<T> Ord for Priority<T> {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.score.cmp(&other.score)
    }
}

use std::{
    collections::{BinaryHeap, HashSet},
    ffi::{c_char, c_void, CStr},
    fs,
};

use ash::vk::Handle;
use vulkan_tutorial::{
    heap::Priority,
    validation::{
        check_validation_layer_support, get_required_extensions, new_debug_messenger_create_info,
        ENABLE_VALIDATION_LAYERS, VALIDATION_LAYERS_CPTR,
    },
    vertex::Vertex,
};

const WIDTH: u32 = 800;
const HEIGHT: u32 = 600;

const DEVICE_EXTENSIONS: [&CStr; 1] = [ash::khr::swapchain::NAME];
pub const DEVICE_EXTENSIONS_CPTR: [*const c_char; DEVICE_EXTENSIONS.len()] = {
    let mut idx = 0;
    let mut arr = [core::ptr::null(); DEVICE_EXTENSIONS.len()];
    while idx < DEVICE_EXTENSIONS.len() {
        arr[idx] = DEVICE_EXTENSIONS[idx].as_ptr();
        idx += 1;
    }
    arr
};
const MAX_FRAMES_IN_FLIGHT: u32 = 2;

pub struct Sdl {
    sdl_context: sdl2::Sdl,
    video_subsystem: sdl2::VideoSubsystem,
    event_subsystem: sdl2::EventSubsystem,
    window: sdl2::video::Window,
}

impl Sdl {
    pub fn new() -> Self {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();
        let window = video_subsystem
            .window("Vulkan", WIDTH, HEIGHT)
            .vulkan()
            .build()
            .unwrap();
        let event_subsystem = sdl_context.event().unwrap();
        Self {
            sdl_context,
            video_subsystem,
            window,
            event_subsystem,
        }
    }
}

#[repr(C)]
pub struct UniformBufferObject {
    model: glm::Mat4,
    view: glm::Mat4,
    proj: glm::Mat4,
}

pub struct Vulkan {
    sdl: Sdl,
    instance: ash::Instance,
    entry: ash::Entry,
    debug_messenger: Option<(
        ash::vk::DebugUtilsMessengerEXT,
        ash::ext::debug_utils::Instance,
    )>,
    surface: (ash::khr::surface::Instance, ash::vk::SurfaceKHR),
    queue_families: QueueFamilyIndices,
    physical_device: ash::vk::PhysicalDevice,
    device: ash::Device,
    graphics_queue: ash::vk::Queue,
    present_queue: ash::vk::Queue,
    swap_chain: (
        ash::khr::swapchain::Device,
        ash::vk::SwapchainKHR,
        ash::vk::Format,
        ash::vk::Extent2D,
    ),
    swap_chain_images: Vec<ash::vk::Image>,
    image_views: Vec<ash::vk::ImageView>,
    render_pass: ash::vk::RenderPass,
    descriptor_pool: ash::vk::DescriptorPool,
    descriptor_set_layout: ash::vk::DescriptorSetLayout,
    descriptor_sets: Vec<ash::vk::DescriptorSet>,
    graphics_pipeline_layout: ash::vk::PipelineLayout,
    graphics_pipeline: ash::vk::Pipeline,
    swap_chain_framebuffers: Vec<ash::vk::Framebuffer>,
    vertex_and_index_buffer: (
        (ash::vk::Buffer, ash::vk::DeviceMemory),
        ash::vk::DeviceSize,
    ),
    uniform_buffers: Vec<(ash::vk::Buffer, ash::vk::DeviceMemory, *mut c_void)>,
    command_pool: ash::vk::CommandPool,
    command_buffers: Vec<ash::vk::CommandBuffer>,
    image_availanble_semaphores: Vec<ash::vk::Semaphore>,
    render_finished_semaphores: Vec<ash::vk::Semaphore>,
    in_fligh_fences: Vec<ash::vk::Fence>,
    frames_in_flight: usize,
    current_frame: usize,
    resize_happened: bool,
    vertices: Vec<Vertex>,
    indices: Vec<u16>,

    start_time: std::time::Instant,
}

impl Vulkan {
    pub fn new(sdl: Sdl, max_frames_in_flight: u32) -> Self {
        let entry = ash::Entry::linked();
        let instance = Self::create_instance(&entry, &sdl.window);
        let debug_messenger = Self::setup_debug_messenger(&instance, &entry);
        let surface = Self::create_surface(&instance, &entry, &sdl.window);
        let physical_device = Self::pick_physical_device(&instance, (&surface.0, surface.1));
        let queue_families =
            QueueFamilyIndices::find_families(physical_device, &instance, (&surface.0, surface.1));
        let (device, graphics_queue, present_queue) =
            Self::create_logical_device(physical_device, &instance, queue_families);
        let (
            swap_chain,
            swap_chain_handle,
            swap_chain_images,
            swap_chain_format,
            swap_chain_extent,
        ) = Self::create_swapchain(
            &instance,
            physical_device,
            &device,
            &sdl.window,
            (&surface.0, surface.1),
        );
        let image_views = Self::create_image_views(&device, &swap_chain_images, swap_chain_format);

        let render_pass = Self::create_render_pass(&device, swap_chain_format);
        let descriptor_set_layout = Self::create_descriptor_set_layout(&device);
        let (graphics_pipeline_layout, graphics_pipeline) = Self::create_graphics_pipeline(
            &device,
            swap_chain_extent,
            descriptor_set_layout,
            render_pass,
        );
        let swap_chain_framebuffers =
            Self::create_framebuffers(&device, render_pass, &image_views, &swap_chain_extent);
        let command_pool =
            Self::create_command_pool(physical_device, &instance, &device, (&surface.0, surface.1));

        let command_buffers =
            Self::create_command_buffers(&device, command_pool, max_frames_in_flight);
        let (image_availanble_semaphores, render_finished_semaphores, in_fligh_fences) =
            Self::create_sync_objects(&device, max_frames_in_flight);

        let vertices = vec![
            Vertex::new(glm::vec2(-0.5, -0.5), glm::vec3(1.0, 0.0, 0.0)),
            Vertex::new(glm::vec2(0.5, -0.5), glm::vec3(0.0, 1.0, 0.0)),
            Vertex::new(glm::vec2(0.5, 0.5), glm::vec3(0.0, 0.0, 1.0)),
            Vertex::new(glm::vec2(-0.5, 0.5), glm::vec3(1.0, 1.0, 1.0)),
        ];

        let indices = vec![0, 1, 2, 2, 3, 0];

        let vertex_and_index_buffer = Self::create_vertex_and_index_buffer(
            &instance,
            &device,
            physical_device,
            command_pool,
            graphics_queue,
            &vertices,
            &indices,
        );

        let uniform_buffers =
            Self::create_uniform_buffers(&instance, &device, physical_device, max_frames_in_flight);

        let descriptor_pool = Self::create_descriptor_pool(&device, max_frames_in_flight);

        let descriptor_sets = Self::create_descriptor_sets(
            &device,
            descriptor_pool,
            max_frames_in_flight,
            descriptor_set_layout,
            &uniform_buffers,
        );
        let start_time = std::time::Instant::now();

        Self {
            sdl,
            instance,
            entry,
            debug_messenger,
            physical_device,
            queue_families,
            device,
            graphics_queue,
            surface,
            present_queue,
            swap_chain: (
                swap_chain,
                swap_chain_handle,
                swap_chain_format,
                swap_chain_extent,
            ),
            swap_chain_images,
            image_views,
            render_pass,
            descriptor_pool,
            descriptor_set_layout,
            descriptor_sets,
            graphics_pipeline_layout,
            graphics_pipeline,
            swap_chain_framebuffers,
            vertex_and_index_buffer,
            uniform_buffers,
            command_pool,
            command_buffers,
            image_availanble_semaphores,
            render_finished_semaphores,
            in_fligh_fences,
            frames_in_flight: max_frames_in_flight as usize,
            current_frame: 0,
            resize_happened: false,
            vertices,
            indices,
            start_time,
        }
    }
    fn create_instance(entry: &ash::Entry, window: &sdl2::video::Window) -> ash::Instance {
        let app_info = ash::vk::ApplicationInfo::default()
            .engine_name(c"Hello Triangle")
            .application_version(ash::vk::make_api_version(0, 1, 0, 0))
            .engine_name(c"No Engine")
            .engine_version(ash::vk::make_api_version(0, 1, 0, 0))
            .api_version(ash::vk::API_VERSION_1_0);

        if ENABLE_VALIDATION_LAYERS && !check_validation_layer_support(entry) {
            panic!("Tried to activate validation layers but they were found missing!");
        }

        let instance_extensions = get_required_extensions(window, entry);
        let mut create_info = ash::vk::InstanceCreateInfo::default()
            .application_info(&app_info)
            .enabled_extension_names(&instance_extensions);

        let mut debug_create_info = new_debug_messenger_create_info();
        if ENABLE_VALIDATION_LAYERS {
            create_info = create_info
                .enabled_layer_names(&VALIDATION_LAYERS_CPTR)
                .push_next(&mut debug_create_info);
        };

        unsafe { entry.create_instance(&create_info, None) }.unwrap()
    }

    fn setup_debug_messenger(
        instance: &ash::Instance,
        entry: &ash::Entry,
    ) -> Option<(
        ash::vk::DebugUtilsMessengerEXT,
        ash::ext::debug_utils::Instance,
    )> {
        if !ENABLE_VALIDATION_LAYERS {
            return None;
        }
        let create_info = new_debug_messenger_create_info();
        let debug_utils = ash::ext::debug_utils::Instance::new(entry, instance);
        let messenger =
            unsafe { debug_utils.create_debug_utils_messenger(&create_info, None) }.unwrap();
        Some((messenger, debug_utils))
    }

    fn create_surface(
        instance: &ash::Instance,
        entry: &ash::Entry,
        window: &sdl2::video::Window,
    ) -> (ash::khr::surface::Instance, ash::vk::SurfaceKHR) {
        (
            ash::khr::surface::Instance::new(entry, instance),
            ash::vk::SurfaceKHR::from_raw(
                window
                    .vulkan_create_surface(instance.handle().as_raw() as _)
                    .unwrap(),
            ),
        )
    }

    fn pick_physical_device(
        instance: &ash::Instance,
        surface: (&ash::khr::surface::Instance, ash::vk::SurfaceKHR),
    ) -> ash::vk::PhysicalDevice {
        let physical_devices = unsafe { instance.enumerate_physical_devices() }.unwrap();
        if physical_devices.len() == 0 {
            panic!("failed to find gpus with vulkan support")
        }

        let mut candidates = BinaryHeap::<Priority<ash::vk::PhysicalDevice>>::new();

        for device in physical_devices {
            let score = Self::rate_device_suitability(device, instance, surface);
            candidates.push(Priority::new(score, device))
        }

        let best = candidates.pop().unwrap();
        if best.get_score() == 0 {
            panic!("failed to find suitable gpu")
        }
        let best = best.get_elem();
        best
    }

    fn rate_device_suitability(
        device: ash::vk::PhysicalDevice,
        instance: &ash::Instance,
        surface: (&ash::khr::surface::Instance, ash::vk::SurfaceKHR),
    ) -> u32 {
        let properties = unsafe { instance.get_physical_device_properties(device) };
        let features = unsafe { instance.get_physical_device_features(device) };

        let mut score = 0;
        if properties.device_type == ash::vk::PhysicalDeviceType::DISCRETE_GPU {
            score += 1_000;
        }

        score += properties.limits.max_image_dimension2_d;
        let indices = QueueFamilyIndices::find_families(device, instance, surface);
        if features.geometry_shader == 0
            || !indices.is_complete()
            || !Self::check_device_extension_support(device, instance)
        {
            return 0;
        }
        let swap_chain_support = SwapChainSupportDetails::new(device, surface);
        if swap_chain_support.formats.is_empty() || swap_chain_support.present_modes.is_empty() {
            return 0;
        }
        score
    }

    fn check_device_extension_support(
        device: ash::vk::PhysicalDevice,
        instance: &ash::Instance,
    ) -> bool {
        let available_extensions =
            unsafe { instance.enumerate_device_extension_properties(device) }.unwrap();
        let mut required_extensions = HashSet::<&CStr>::from(DEVICE_EXTENSIONS);
        for extension in available_extensions {
            required_extensions
                .remove(unsafe { CStr::from_ptr(extension.extension_name.as_ptr()) });
        }
        required_extensions.is_empty()
    }

    fn create_logical_device(
        device: ash::vk::PhysicalDevice,
        instance: &ash::Instance,
        indices: QueueFamilyIndices,
    ) -> (ash::Device, ash::vk::Queue, ash::vk::Queue) {
        let mut queue_create_infos: Vec<ash::vk::DeviceQueueCreateInfo> = vec![];
        let unique_queue_families: HashSet<u32> = HashSet::from([
            indices.graphics_family.unwrap(),
            indices.present_family.unwrap(),
        ]);
        let queue_priority = [1.0];
        for queue_family in unique_queue_families {
            let mut queue_create_info = ash::vk::DeviceQueueCreateInfo::default()
                .queue_family_index(queue_family)
                .queue_priorities(&queue_priority);
            queue_create_info.queue_count = 1;
            queue_create_infos.push(queue_create_info);
        }

        let device_features = ash::vk::PhysicalDeviceFeatures::default();
        let mut create_info = ash::vk::DeviceCreateInfo::default()
            .queue_create_infos(&queue_create_infos)
            .enabled_features(&device_features);
        if ENABLE_VALIDATION_LAYERS {
            create_info = create_info.enabled_layer_names(&VALIDATION_LAYERS_CPTR);
        }
        let create_info = create_info.enabled_extension_names(&DEVICE_EXTENSIONS_CPTR);
        let device = unsafe { instance.create_device(device, &create_info, None) }.unwrap();
        let queue = unsafe { device.get_device_queue(indices.graphics_family.unwrap(), 0) };
        let present = unsafe { device.get_device_queue(indices.present_family.unwrap(), 0) };
        (device, queue, present)
    }

    fn create_swapchain(
        instance: &ash::Instance,
        physical_device: ash::vk::PhysicalDevice,
        device: &ash::Device,
        window: &sdl2::video::Window,
        surface: (&ash::khr::surface::Instance, ash::vk::SurfaceKHR),
    ) -> (
        ash::khr::swapchain::Device,
        ash::vk::SwapchainKHR,
        Vec<ash::vk::Image>,
        ash::vk::Format,
        ash::vk::Extent2D,
    ) {
        let swap_chain_support = SwapChainSupportDetails::new(physical_device, surface);
        let surface_format = Self::choose_swap_surface_format(swap_chain_support.formats());
        let present_mode = Self::choose_swap_present_mode(swap_chain_support.present_modes());
        let extent = Self::choose_swap_extent(&swap_chain_support.capabilities(), window);

        let image_count = if swap_chain_support.capabilities().max_image_count == 0 {
            swap_chain_support.capabilities().min_image_count + 1
        } else {
            swap_chain_support.capabilities().min_image_count
                + 1.clamp(
                    swap_chain_support.capabilities().min_image_count,
                    swap_chain_support.capabilities().max_image_count,
                )
        };

        let mut create_info = ash::vk::SwapchainCreateInfoKHR::default()
            .surface(surface.1)
            .min_image_count(image_count)
            .image_format(surface_format.format)
            .image_color_space(surface_format.color_space)
            .image_extent(extent)
            .image_array_layers(1)
            .image_usage(ash::vk::ImageUsageFlags::COLOR_ATTACHMENT);

        let indices = QueueFamilyIndices::find_families(physical_device, instance, surface);
        let queue_family_indices = [
            indices.graphics_family.unwrap(),
            indices.present_family.unwrap(),
        ];
        create_info = if indices.graphics_family != indices.present_family {
            create_info
                .image_sharing_mode(ash::vk::SharingMode::CONCURRENT)
                .queue_family_indices(&queue_family_indices)
        } else {
            create_info.image_sharing_mode(ash::vk::SharingMode::EXCLUSIVE)
        };

        let create_info = create_info
            .pre_transform(swap_chain_support.capabilities().current_transform)
            .composite_alpha(ash::vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true)
            .old_swapchain(ash::vk::SwapchainKHR::null());

        let swap_chain = ash::khr::swapchain::Device::new(instance, device);
        let handle = unsafe { swap_chain.create_swapchain(&create_info, None).unwrap() };
        let swap_chain_images = unsafe { swap_chain.get_swapchain_images(handle).unwrap() };

        (
            swap_chain,
            handle,
            swap_chain_images,
            surface_format.format,
            extent,
        )
    }

    fn recreate_swapchain(&mut self) {
        while self.sdl().window.is_minimized() {
            self.sdl().sdl_context.event_pump().unwrap().wait_event();
        }
        self.wait_idle();
        self.drop_swapchain();
        let swapchain = Self::create_swapchain(
            &self.instance,
            self.physical_device,
            &self.device,
            &self.sdl.window,
            (&self.surface.0, self.surface.1),
        );
        let image_views = Self::create_image_views(&self.device, &swapchain.2, swapchain.3);
        let framebuffers =
            Self::create_framebuffers(&self.device, self.render_pass, &image_views, &swapchain.4);
        self.swap_chain_images = swapchain.2;
        self.swap_chain = (swapchain.0, swapchain.1, swapchain.3, swapchain.4);
        self.swap_chain_framebuffers = framebuffers;
        self.image_views = image_views;
    }

    fn drop_swapchain(&mut self) {
        unsafe {
            self.swap_chain_framebuffers
                .iter()
                .for_each(|buffer| self.device.destroy_framebuffer(*buffer, None));

            self.image_views
                .iter()
                .for_each(|image_view| self.device.destroy_image_view(*image_view, None));
            self.swap_chain.0.destroy_swapchain(self.swap_chain.1, None);
        }
    }

    fn choose_swap_surface_format(
        available_formats: &[ash::vk::SurfaceFormatKHR],
    ) -> ash::vk::SurfaceFormatKHR {
        for available_format in available_formats {
            if available_format.format == ash::vk::Format::B8G8R8A8_SRGB
                && available_format.color_space == ash::vk::ColorSpaceKHR::SRGB_NONLINEAR
            {
                return *available_format;
            }
        }
        available_formats[0]
    }

    fn choose_swap_present_mode(
        available_present_modes: &[ash::vk::PresentModeKHR],
    ) -> ash::vk::PresentModeKHR {
        for available_present_mode in available_present_modes {
            if *available_present_mode == ash::vk::PresentModeKHR::MAILBOX {
                return *available_present_mode;
            }
        }
        ash::vk::PresentModeKHR::FIFO
    }

    fn choose_swap_extent(
        capabilities: &ash::vk::SurfaceCapabilitiesKHR,
        window: &sdl2::video::Window,
    ) -> ash::vk::Extent2D {
        if capabilities.current_extent.width != u32::MAX {
            capabilities.current_extent
        } else {
            let (width, height) = window.vulkan_drawable_size();
            ash::vk::Extent2D {
                height: height.clamp(
                    capabilities.min_image_extent.height,
                    capabilities.max_image_extent.height,
                ),
                width: width.clamp(
                    capabilities.min_image_extent.width,
                    capabilities.max_image_extent.width,
                ),
            }
        }
    }
    fn create_image_views(
        device: &ash::Device,
        swap_chain_images: &[ash::vk::Image],
        swap_chain_image_format: ash::vk::Format,
    ) -> Vec<ash::vk::ImageView> {
        let mut image_views = Vec::with_capacity(swap_chain_images.len());
        for (idx, image) in swap_chain_images.iter().enumerate() {
            let create_info = ash::vk::ImageViewCreateInfo::default()
                .image(*image)
                .view_type(ash::vk::ImageViewType::TYPE_2D)
                .format(swap_chain_image_format)
                .components(
                    ash::vk::ComponentMapping::default()
                        .r(ash::vk::ComponentSwizzle::IDENTITY)
                        .g(ash::vk::ComponentSwizzle::IDENTITY)
                        .b(ash::vk::ComponentSwizzle::IDENTITY)
                        .a(ash::vk::ComponentSwizzle::IDENTITY),
                )
                .subresource_range(
                    ash::vk::ImageSubresourceRange::default()
                        .aspect_mask(ash::vk::ImageAspectFlags::COLOR)
                        .base_mip_level(0)
                        .level_count(1)
                        .base_array_layer(0)
                        .layer_count(1),
                );
            image_views.push(unsafe { device.create_image_view(&create_info, None).unwrap() });
        }
        image_views
    }

    fn create_graphics_pipeline(
        device: &ash::Device,
        swap_chain_extent: ash::vk::Extent2D,
        descriptor_layout: ash::vk::DescriptorSetLayout,
        render_pass: ash::vk::RenderPass,
    ) -> (ash::vk::PipelineLayout, ash::vk::Pipeline) {
        let vert_shader_code = fs::read(format!("{}/vert.spv", std::env!("OUT_DIR"))).unwrap();
        let frag_shader_code = fs::read(format!("{}/frag.spv", std::env!("OUT_DIR"))).unwrap();

        let vert_shader_module = Self::create_shader_module(device, &vert_shader_code);
        let frag_shader_module = Self::create_shader_module(device, &frag_shader_code);

        let vert_shader_stage_info = ash::vk::PipelineShaderStageCreateInfo::default()
            .stage(ash::vk::ShaderStageFlags::VERTEX)
            .module(vert_shader_module)
            .name(c"main");
        let frag_shader_stage_info = ash::vk::PipelineShaderStageCreateInfo::default()
            .stage(ash::vk::ShaderStageFlags::FRAGMENT)
            .module(frag_shader_module)
            .name(c"main");

        let shader_stages = [vert_shader_stage_info, frag_shader_stage_info];
        let dynamic_states = [
            ash::vk::DynamicState::VIEWPORT,
            ash::vk::DynamicState::SCISSOR,
        ];

        let dynamic_state =
            ash::vk::PipelineDynamicStateCreateInfo::default().dynamic_states(&dynamic_states);

        let binding_description = [Vertex::binding_description()];
        let attribute_description = Vertex::attribute_descriptions();
        let vertex_input_info = ash::vk::PipelineVertexInputStateCreateInfo::default()
            .vertex_binding_descriptions(&binding_description)
            .vertex_attribute_descriptions(&attribute_description);

        let input_assembly = ash::vk::PipelineInputAssemblyStateCreateInfo::default()
            .topology(ash::vk::PrimitiveTopology::TRIANGLE_LIST);

        let viewport = ash::vk::Viewport::default()
            .x(0.0)
            .y(0.0)
            .width(swap_chain_extent.width as _)
            .height(swap_chain_extent.height as _)
            .min_depth(0.0)
            .max_depth(1.0);

        let scissor = ash::vk::Rect2D::default()
            .offset(ash::vk::Offset2D::default().x(0).y(0))
            .extent(swap_chain_extent);

        let viewport_state = ash::vk::PipelineViewportStateCreateInfo::default()
            .viewport_count(1)
            .scissor_count(1);

        let rasterizer = ash::vk::PipelineRasterizationStateCreateInfo::default()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(ash::vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(ash::vk::CullModeFlags::BACK)
            .front_face(ash::vk::FrontFace::COUNTER_CLOCKWISE)
            .depth_bias_enable(false);

        let multisampling = ash::vk::PipelineMultisampleStateCreateInfo::default()
            .sample_shading_enable(false)
            .rasterization_samples(ash::vk::SampleCountFlags::TYPE_1);

        let color_blend_attachment = ash::vk::PipelineColorBlendAttachmentState::default()
            .color_write_mask(ash::vk::ColorComponentFlags::RGBA)
            .blend_enable(false);

        let color_blend_attachments = [color_blend_attachment];

        let color_blending = ash::vk::PipelineColorBlendStateCreateInfo::default()
            .logic_op_enable(false)
            .attachments(&color_blend_attachments);

        let descriptor_layouts = [descriptor_layout];

        let pipeline_layout =
            ash::vk::PipelineLayoutCreateInfo::default().set_layouts(&descriptor_layouts);

        let pipeline_layout = unsafe {
            device
                .create_pipeline_layout(&pipeline_layout, None)
                .unwrap()
        };

        let create_info = ash::vk::GraphicsPipelineCreateInfo::default()
            .stages(&shader_stages)
            .vertex_input_state(&vertex_input_info)
            .input_assembly_state(&input_assembly)
            .viewport_state(&viewport_state)
            .rasterization_state(&rasterizer)
            .multisample_state(&multisampling)
            .dynamic_state(&dynamic_state)
            .color_blend_state(&color_blending)
            .layout(pipeline_layout)
            .render_pass(render_pass)
            .subpass(0);

        let create_infos = [create_info];
        let pipelines = unsafe {
            device
                .create_graphics_pipelines(ash::vk::PipelineCache::null(), &create_infos, None)
                .unwrap()
        };

        unsafe {
            device.destroy_shader_module(vert_shader_module, None);
            device.destroy_shader_module(frag_shader_module, None);
        }

        (pipeline_layout, pipelines[0])
    }

    fn create_shader_module(device: &ash::Device, code: &[u8]) -> ash::vk::ShaderModule {
        if code.len() % 4 != 0 {
            panic!("shader byte code not aligned to doublewords")
        }
        let create_info = ash::vk::ShaderModuleCreateInfo::default()
            // i know that this is disgusting but find a better way
            .code(unsafe {
                std::slice::from_raw_parts(code.as_ptr() as *const u32, code.len() / 4)
            });
        unsafe { device.create_shader_module(&create_info, None) }.unwrap()
    }

    fn create_render_pass(
        device: &ash::Device,
        swap_chain_image_format: ash::vk::Format,
    ) -> ash::vk::RenderPass {
        let color_attachment = ash::vk::AttachmentDescription::default()
            .format(swap_chain_image_format)
            .samples(ash::vk::SampleCountFlags::TYPE_1)
            .load_op(ash::vk::AttachmentLoadOp::CLEAR)
            .store_op(ash::vk::AttachmentStoreOp::STORE)
            .stencil_load_op(ash::vk::AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(ash::vk::AttachmentStoreOp::DONT_CARE)
            .initial_layout(ash::vk::ImageLayout::UNDEFINED)
            .final_layout(ash::vk::ImageLayout::PRESENT_SRC_KHR);

        let color_attachment_ref = ash::vk::AttachmentReference::default()
            .attachment(0)
            .layout(ash::vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL);

        let color_attachment_refs = [color_attachment_ref];
        let subpass = ash::vk::SubpassDescription::default()
            .pipeline_bind_point(ash::vk::PipelineBindPoint::GRAPHICS)
            .color_attachments(&color_attachment_refs);

        let dependency = ash::vk::SubpassDependency::default()
            .src_subpass(ash::vk::SUBPASS_EXTERNAL)
            .dst_subpass(0)
            .src_stage_mask(ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .src_access_mask(ash::vk::AccessFlags::NONE)
            .dst_stage_mask(ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .dst_access_mask(ash::vk::AccessFlags::COLOR_ATTACHMENT_WRITE);
        let color_attachments = [color_attachment];
        let sub_passes = [subpass];
        let dependiencies = [dependency];

        let create_info = ash::vk::RenderPassCreateInfo::default()
            .attachments(&color_attachments)
            .subpasses(&sub_passes)
            .dependencies(&dependiencies);

        unsafe { device.create_render_pass(&create_info, None).unwrap() }
    }

    fn create_framebuffers(
        device: &ash::Device,
        render_pass: ash::vk::RenderPass,
        swap_chain_image_views: &[ash::vk::ImageView],
        swap_chain_extent: &ash::vk::Extent2D,
    ) -> Vec<ash::vk::Framebuffer> {
        swap_chain_image_views
            .into_iter()
            .map(|image_view| {
                let image_view = [*image_view];
                let create_info = ash::vk::FramebufferCreateInfo::default()
                    .render_pass(render_pass)
                    .attachments(&image_view)
                    .width(swap_chain_extent.width)
                    .height(swap_chain_extent.height)
                    .width(swap_chain_extent.width)
                    .height(swap_chain_extent.height)
                    .layers(1);
                unsafe { device.create_framebuffer(&create_info, None).unwrap() }
            })
            .collect()
    }

    fn create_command_pool(
        physical_device: ash::vk::PhysicalDevice,
        instance: &ash::Instance,
        device: &ash::Device,
        surface: (&ash::khr::surface::Instance, ash::vk::SurfaceKHR),
    ) -> ash::vk::CommandPool {
        let queue_family_indices =
            QueueFamilyIndices::find_families(physical_device, instance, surface);
        let pool_info = ash::vk::CommandPoolCreateInfo::default()
            .flags(ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(queue_family_indices.graphics_family.unwrap());
        unsafe { device.create_command_pool(&pool_info, None).unwrap() }
    }

    fn create_command_buffers(
        device: &ash::Device,
        command_pool: ash::vk::CommandPool,
        frames_in_flight: u32,
    ) -> Vec<ash::vk::CommandBuffer> {
        let alloc_info = ash::vk::CommandBufferAllocateInfo::default()
            .command_pool(command_pool)
            .level(ash::vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(frames_in_flight);
        unsafe { device.allocate_command_buffers(&alloc_info).unwrap() }
    }

    pub fn sdl(&self) -> &Sdl {
        &self.sdl
    }

    fn record_command_buffer(&self, command_buffer: ash::vk::CommandBuffer, image_index: usize) {
        let begin_info = ash::vk::CommandBufferBeginInfo::default();

        unsafe {
            self.device
                .begin_command_buffer(command_buffer, &begin_info)
                .unwrap();
            let clear_color = [ash::vk::ClearValue {
                color: ash::vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            }];
            let render_pass_info = ash::vk::RenderPassBeginInfo::default()
                .render_pass(self.render_pass)
                .framebuffer(self.swap_chain_framebuffers[image_index])
                .render_area(
                    ash::vk::Rect2D::default()
                        .offset(ash::vk::Offset2D { x: 0, y: 0 })
                        .extent(self.swap_chain.3),
                )
                .clear_values(&clear_color);

            self.device.cmd_begin_render_pass(
                command_buffer,
                &render_pass_info,
                ash::vk::SubpassContents::INLINE,
            );
            {
                self.device.cmd_bind_pipeline(
                    command_buffer,
                    ash::vk::PipelineBindPoint::GRAPHICS,
                    self.graphics_pipeline,
                );
                let viewport = ash::vk::Viewport::default()
                    .x(0.0)
                    .y(0.0)
                    .width(self.swap_chain.3.width as _)
                    .height(self.swap_chain.3.height as _)
                    .min_depth(0.0)
                    .max_depth(1.0);
                self.device.cmd_set_viewport(command_buffer, 0, &[viewport]);
                let scissor = ash::vk::Rect2D::default()
                    .offset(ash::vk::Offset2D { x: 0, y: 0 })
                    .extent(self.swap_chain.3);

                self.device.cmd_set_scissor(command_buffer, 0, &[scissor]);

                let vertex_buffers = [self.vertex_and_index_buffer.0 .0];
                let device_offsets = [0];
                self.device.cmd_bind_vertex_buffers(
                    command_buffer,
                    0,
                    &vertex_buffers,
                    &device_offsets,
                );

                self.device.cmd_bind_index_buffer(
                    command_buffer,
                    self.vertex_and_index_buffer.0 .0,
                    self.vertex_and_index_buffer.1,
                    ash::vk::IndexType::UINT16,
                );

                let descriptor_set = [self.descriptor_sets[self.current_frame]];
                self.device.cmd_bind_descriptor_sets(
                    command_buffer,
                    ash::vk::PipelineBindPoint::GRAPHICS,
                    self.graphics_pipeline_layout,
                    0,
                    &descriptor_set,
                    &[],
                );

                self.device
                    .cmd_draw_indexed(command_buffer, self.indices.len() as u32, 1, 0, 0, 0);
            }
            self.device.cmd_end_render_pass(command_buffer);
            self.device.end_command_buffer(command_buffer).unwrap()
        }
    }

    pub fn draw_frame(&mut self) {
        unsafe {
            self.device
                .wait_for_fences(&[self.in_fligh_fences[self.current_frame]], true, u64::MAX)
                .unwrap();
            let (image_index, _) = match self.swap_chain.0.acquire_next_image(
                self.swap_chain.1,
                u64::MAX,
                self.image_availanble_semaphores[self.current_frame],
                ash::vk::Fence::null(),
            ) {
                Ok(ok) => ok,
                Err(err) => match err {
                    ash::vk::Result::ERROR_OUT_OF_DATE_KHR => {
                        self.recreate_swapchain();
                        return;
                    }
                    _ => {
                        panic!("failed to acqure swap chain image")
                    }
                },
            };
            self.device
                .reset_fences(&[self.in_fligh_fences[self.current_frame]])
                .unwrap();

            self.device
                .reset_command_buffer(
                    self.command_buffers[self.current_frame],
                    ash::vk::CommandBufferResetFlags::from_raw(0),
                )
                .unwrap();
            self.record_command_buffer(
                self.command_buffers[self.current_frame],
                image_index as usize,
            );

            self.update_uniform_buffer(self.current_frame);

            let wait_semaphores = [self.image_availanble_semaphores[self.current_frame]];
            let wait_stages = [ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
            let signal_semaphores = [self.render_finished_semaphores[self.current_frame]];
            let command_buffers = [self.command_buffers[self.current_frame]];
            let submit_info = ash::vk::SubmitInfo::default()
                .wait_semaphores(&wait_semaphores)
                .wait_dst_stage_mask(&wait_stages)
                .command_buffers(&command_buffers)
                .signal_semaphores(&signal_semaphores);
            let _ = self
                .device
                .queue_submit(
                    self.graphics_queue,
                    &[submit_info],
                    self.in_fligh_fences[self.current_frame],
                )
                .unwrap();

            let swap_chains = [self.swap_chain.1];
            let image_indices = [image_index];
            let present_info = ash::vk::PresentInfoKHR::default()
                .wait_semaphores(&signal_semaphores)
                .swapchains(&swap_chains)
                .image_indices(&image_indices);
            let result = self
                .swap_chain
                .0
                .queue_present(self.present_queue, &present_info);
            if self.resize_happened {
                self.resize_happened = false;
                self.recreate_swapchain();
            } else {
                match result {
                    Ok(suitable) => {
                        if !suitable {
                            self.resize_happened = false;
                            self.recreate_swapchain();
                        }
                    }
                    Err(err) => match err {
                        ash::vk::Result::ERROR_OUT_OF_DATE_KHR => {
                            self.resize_happened = false;
                            self.recreate_swapchain()
                        }
                        _ => panic!("failed to present to swap chain image"),
                    },
                }
            }

            self.current_frame = (self.current_frame + 1) % self.frames_in_flight;
        }
    }

    fn create_sync_objects(
        device: &ash::Device,
        frames_in_flight: u32,
    ) -> (
        Vec<ash::vk::Semaphore>,
        Vec<ash::vk::Semaphore>,
        Vec<ash::vk::Fence>,
    ) {
        let semaphore_info = ash::vk::SemaphoreCreateInfo::default();
        let fence_info =
            ash::vk::FenceCreateInfo::default().flags(ash::vk::FenceCreateFlags::SIGNALED);

        let mut image_semaphores = Vec::with_capacity(frames_in_flight as usize);
        let mut render_semaphores = Vec::with_capacity(frames_in_flight as usize);
        let mut fences = Vec::with_capacity(frames_in_flight as usize);
        for _ in 0..frames_in_flight {
            unsafe {
                image_semaphores.push(device.create_semaphore(&semaphore_info, None).unwrap());
                render_semaphores.push(device.create_semaphore(&semaphore_info, None).unwrap());
                fences.push(device.create_fence(&fence_info, None).unwrap());
            }
        }
        (image_semaphores, render_semaphores, fences)
    }

    pub fn wait_idle(&self) {
        unsafe { self.device.device_wait_idle() }.unwrap()
    }

    fn find_memory_type(
        instance: &ash::Instance,
        device: ash::vk::PhysicalDevice,
        type_filter: u32,
        properties: ash::vk::MemoryPropertyFlags,
    ) -> u32 {
        let mem_properties = unsafe { instance.get_physical_device_memory_properties(device) };

        for i in 0..mem_properties.memory_type_count {
            if type_filter & (1 << i) > 0
                && mem_properties.memory_types[i as usize]
                    .property_flags
                    .contains(properties)
            {
                return i;
            }
        }
        panic!("Couldn't find memory type")
    }

    fn create_buffer(
        instance: &ash::Instance,
        device: &ash::Device,
        physical_device: ash::vk::PhysicalDevice,
        size: ash::vk::DeviceSize,
        usage: ash::vk::BufferUsageFlags,
        properties: ash::vk::MemoryPropertyFlags,
    ) -> Result<(ash::vk::Buffer, ash::vk::DeviceMemory), &'static str> {
        let buffer_info = ash::vk::BufferCreateInfo::default()
            .size(size)
            .usage(usage)
            .sharing_mode(ash::vk::SharingMode::EXCLUSIVE);
        let Ok(buffer) = (unsafe { device.create_buffer(&buffer_info, None) }) else {
            return Err("Failed to create buffer");
        };

        let mem_requirements = unsafe { device.get_buffer_memory_requirements(buffer) };

        let alloc_info = ash::vk::MemoryAllocateInfo::default()
            .allocation_size(mem_requirements.size)
            .memory_type_index(Self::find_memory_type(
                instance,
                physical_device,
                mem_requirements.memory_type_bits,
                properties,
            ));

        let Ok(buffer_memory) = (unsafe { device.allocate_memory(&alloc_info, None) }) else {
            return Err("failed to alloate buffer");
        };

        unsafe { device.bind_buffer_memory(buffer, buffer_memory, 0) }.unwrap();
        Ok((buffer, buffer_memory))
    }

    fn copy_buffer(
        device: &ash::Device,
        graphics_queue: ash::vk::Queue,
        command_pool: ash::vk::CommandPool,
        src_buffer: ash::vk::Buffer,
        dst_buffer: ash::vk::Buffer,
        size: ash::vk::DeviceSize,
    ) {
        let alloc_info = ash::vk::CommandBufferAllocateInfo::default()
            .level(ash::vk::CommandBufferLevel::PRIMARY)
            .command_pool(command_pool)
            .command_buffer_count(1);

        let command_buffer = unsafe { device.allocate_command_buffers(&alloc_info) }.unwrap();

        let begin_info = ash::vk::CommandBufferBeginInfo::default()
            .flags(ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        unsafe {
            device
                .begin_command_buffer(command_buffer[0], &begin_info)
                .unwrap()
        };

        let copy_region = ash::vk::BufferCopy::default()
            .src_offset(0)
            .dst_offset(0)
            .size(size);

        unsafe {
            device.cmd_copy_buffer(command_buffer[0], src_buffer, dst_buffer, &[copy_region]);
            device.end_command_buffer(command_buffer[0]).unwrap();
        }

        let submit_info = ash::vk::SubmitInfo::default().command_buffers(&command_buffer);

        unsafe {
            device
                .queue_submit(graphics_queue, &[submit_info], ash::vk::Fence::null())
                .unwrap();
            device.queue_wait_idle(graphics_queue).unwrap();
            device.free_command_buffers(command_pool, &command_buffer);
        }
    }

    fn create_vertex_and_index_buffer(
        instance: &ash::Instance,
        device: &ash::Device,
        physical_device: ash::vk::PhysicalDevice,
        command_pool: ash::vk::CommandPool,
        graphics_queue: ash::vk::Queue,
        vertices: &[Vertex],
        indices: &[u16],
    ) -> (
        (ash::vk::Buffer, ash::vk::DeviceMemory),
        ash::vk::DeviceSize,
    ) {
        let index_size = (indices.len() * std::mem::size_of::<u16>()) as u64;
        let buffer_size = (vertices.len() * std::mem::size_of::<Vertex>()) as u64 + index_size;

        let staging_buffer = Self::create_buffer(
            instance,
            device,
            physical_device,
            buffer_size,
            ash::vk::BufferUsageFlags::TRANSFER_SRC,
            ash::vk::MemoryPropertyFlags::HOST_VISIBLE
                | ash::vk::MemoryPropertyFlags::HOST_COHERENT,
        )
        .unwrap();

        unsafe {
            let mut data = device
                .map_memory(
                    staging_buffer.1,
                    0,
                    buffer_size - index_size,
                    ash::vk::MemoryMapFlags::empty(),
                )
                .unwrap();
            std::ptr::copy_nonoverlapping(
                vertices.as_ptr() as *const c_void,
                data,
                (buffer_size - index_size) as usize,
            );
            device.unmap_memory(staging_buffer.1);
            data = device
                .map_memory(
                    staging_buffer.1,
                    buffer_size - index_size,
                    index_size,
                    ash::vk::MemoryMapFlags::empty(),
                )
                .unwrap();
            std::ptr::copy_nonoverlapping(
                indices.as_ptr() as *const c_void,
                data,
                index_size as usize,
            );
            device.unmap_memory(staging_buffer.1);
        }

        let buffer = Self::create_buffer(
            instance,
            device,
            physical_device,
            buffer_size,
            ash::vk::BufferUsageFlags::INDEX_BUFFER
                | ash::vk::BufferUsageFlags::VERTEX_BUFFER
                | ash::vk::BufferUsageFlags::TRANSFER_DST,
            ash::vk::MemoryPropertyFlags::DEVICE_LOCAL,
        )
        .unwrap();

        Self::copy_buffer(
            device,
            graphics_queue,
            command_pool,
            staging_buffer.0,
            buffer.0,
            buffer_size,
        );
        unsafe {
            device.destroy_buffer(staging_buffer.0, None);
            device.free_memory(staging_buffer.1, None);
        }

        (buffer, buffer_size - index_size)
    }

    fn create_descriptor_set_layout(device: &ash::Device) -> ash::vk::DescriptorSetLayout {
        let layout_bindings = [ash::vk::DescriptorSetLayoutBinding::default()
            .binding(0)
            .descriptor_type(ash::vk::DescriptorType::UNIFORM_BUFFER)
            .descriptor_count(1)
            .stage_flags(ash::vk::ShaderStageFlags::VERTEX)];
        let layout_info =
            ash::vk::DescriptorSetLayoutCreateInfo::default().bindings(&layout_bindings);

        unsafe {
            device
                .create_descriptor_set_layout(&layout_info, None)
                .unwrap()
        }
    }

    fn create_uniform_buffers(
        instance: &ash::Instance,
        device: &ash::Device,
        physical_device: ash::vk::PhysicalDevice,
        frames_in_flight: u32,
    ) -> Vec<(ash::vk::Buffer, ash::vk::DeviceMemory, *mut c_void)> {
        let buffer_size = std::mem::size_of::<UniformBufferObject>() as u64;
        let mut ubos = Vec::with_capacity(frames_in_flight as usize);

        for _ in 0..frames_in_flight {
            let (buffer, memory) = Self::create_buffer(
                instance,
                device,
                physical_device,
                buffer_size,
                ash::vk::BufferUsageFlags::UNIFORM_BUFFER,
                ash::vk::MemoryPropertyFlags::HOST_VISIBLE
                    | ash::vk::MemoryPropertyFlags::HOST_COHERENT,
            )
            .unwrap();

            let data = unsafe {
                device
                    .map_memory(memory, 0, buffer_size, ash::vk::MemoryMapFlags::empty())
                    .unwrap()
            };
            ubos.push((buffer, memory, data))
        }

        ubos
    }

    fn update_uniform_buffer(&self, current_frame: usize) {
        let time = self.start_time.elapsed().as_secs_f32();

        let mut ubo = UniformBufferObject {
            model: glm::ext::rotate(
                &glm::mat4(
                    1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0,
                ),
                time * glm::radians(90.0),
                glm::vec3(0.0, 0.0, 1.0),
            ),
            view: glm::ext::look_at(
                glm::vec3(2.0, 2.0, 2.0),
                glm::vec3(0.0, 0.0, 0.0),
                glm::vec3(0.0, 0.0, 1.0),
            ),
            proj: glm::ext::perspective(
                glm::radians(45.0),
                self.swap_chain.3.width as f32 / self.swap_chain.3.height as f32,
                0.1,
                10.0,
            ),
        };

        ubo.proj[1][1] *= -1.0;

        unsafe {
            std::ptr::copy_nonoverlapping(
                &ubo as *const UniformBufferObject as *const c_void,
                self.uniform_buffers[self.current_frame].2,
                std::mem::size_of::<UniformBufferObject>(),
            );
        }
    }

    fn create_descriptor_pool(
        device: &ash::Device,
        max_frames_in_flight: u32,
    ) -> ash::vk::DescriptorPool {
        let pool_size = [ash::vk::DescriptorPoolSize::default()
            .ty(ash::vk::DescriptorType::UNIFORM_BUFFER)
            .descriptor_count(max_frames_in_flight)];
        let pool_info = ash::vk::DescriptorPoolCreateInfo::default()
            .pool_sizes(&pool_size)
            .max_sets(max_frames_in_flight);
        unsafe { device.create_descriptor_pool(&pool_info, None).unwrap() }
    }

    fn create_descriptor_sets(
        device: &ash::Device,
        descriptor_pool: ash::vk::DescriptorPool,
        max_frames_in_flight: u32,
        descriptor_set_layout: ash::vk::DescriptorSetLayout,
        uniform_buffers: &[(ash::vk::Buffer, ash::vk::DeviceMemory, *mut c_void)],
    ) -> Vec<ash::vk::DescriptorSet> {
        let layouts = vec![descriptor_set_layout; max_frames_in_flight as usize];

        let alloc_info = ash::vk::DescriptorSetAllocateInfo::default()
            .descriptor_pool(descriptor_pool)
            .set_layouts(&layouts);

        let sets = unsafe { device.allocate_descriptor_sets(&alloc_info).unwrap() };
        for i in 0..sets.len() {
            let descriptor_info = [ash::vk::DescriptorBufferInfo::default()
                .buffer(uniform_buffers[i].0)
                .offset(0)
                .range(std::mem::size_of::<UniformBufferObject>() as u64)];
            let descriptor_write = ash::vk::WriteDescriptorSet::default()
                .dst_set(sets[i])
                .dst_binding(0)
                .dst_array_element(0)
                .descriptor_type(ash::vk::DescriptorType::UNIFORM_BUFFER)
                .buffer_info(&descriptor_info);

            unsafe { device.update_descriptor_sets(&[descriptor_write], &[]) };
        }
        sets
    }
}

impl Drop for Vulkan {
    fn drop(&mut self) {
        unsafe {
            self.drop_swapchain();

            for (buffer, memory, _) in &self.uniform_buffers {
                self.device.destroy_buffer(*buffer, None);
                self.device.free_memory(*memory, None);
            }

            self.device
                .destroy_descriptor_pool(self.descriptor_pool, None);
            self.device
                .destroy_descriptor_set_layout(self.descriptor_set_layout, None);

            self.device
                .destroy_buffer(self.vertex_and_index_buffer.0 .0, None);
            self.device
                .free_memory(self.vertex_and_index_buffer.0 .1, None);
            for i in 0..self.frames_in_flight {
                self.device
                    .destroy_semaphore(self.image_availanble_semaphores[i], None);
                self.device
                    .destroy_semaphore(self.render_finished_semaphores[i], None);
                self.device.destroy_fence(self.in_fligh_fences[i], None);
            }

            self.device.destroy_command_pool(self.command_pool, None);

            self.device.destroy_pipeline(self.graphics_pipeline, None);
            self.device
                .destroy_pipeline_layout(self.graphics_pipeline_layout, None);
            self.device.destroy_render_pass(self.render_pass, None);

            self.device.destroy_device(None);

            if let Some(debug_messenger) = &self.debug_messenger {
                debug_messenger
                    .1
                    .destroy_debug_utils_messenger(debug_messenger.0, None)
            }
            self.surface.0.destroy_surface(self.surface.1, None);
            self.instance.destroy_instance(None);
        };
    }
}

#[derive(Clone, Copy, Debug)]
struct QueueFamilyIndices {
    graphics_family: Option<u32>,
    present_family: Option<u32>,
}

impl QueueFamilyIndices {
    pub fn find_families(
        device: ash::vk::PhysicalDevice,
        instance: &ash::Instance,
        surface: (&ash::khr::surface::Instance, ash::vk::SurfaceKHR),
    ) -> Self {
        let mut graphics = None;
        let mut present = None;
        let queue_families =
            unsafe { instance.get_physical_device_queue_family_properties(device) };

        let mut i = 0;
        for queue_family in queue_families.iter() {
            if queue_family
                .queue_flags
                .contains(ash::vk::QueueFlags::GRAPHICS)
            {
                graphics = Some(i);
            }
            if let Ok(_) = unsafe {
                surface
                    .0
                    .get_physical_device_surface_support(device, i, surface.1)
            } {
                present = Some(i);
            }

            if graphics.is_some() && present.is_some() {
                break;
            }
            i += 1;
        }

        QueueFamilyIndices {
            graphics_family: graphics,
            present_family: present,
        }
    }
    pub fn is_complete(&self) -> bool {
        self.graphics_family.is_some() && self.present_family.is_some()
    }
}

struct SwapChainSupportDetails {
    capabilities: ash::vk::SurfaceCapabilitiesKHR,
    formats: Vec<ash::vk::SurfaceFormatKHR>,
    present_modes: Vec<ash::vk::PresentModeKHR>,
}

impl SwapChainSupportDetails {
    fn new(
        device: ash::vk::PhysicalDevice,
        surface: (&ash::khr::surface::Instance, ash::vk::SurfaceKHR),
    ) -> Self {
        let capabilities = unsafe {
            surface
                .0
                .get_physical_device_surface_capabilities(device, surface.1)
        }
        .unwrap();
        let formats = unsafe {
            surface
                .0
                .get_physical_device_surface_formats(device, surface.1)
                .unwrap()
        };
        let present_modes = unsafe {
            surface
                .0
                .get_physical_device_surface_present_modes(device, surface.1)
                .unwrap()
        };
        Self {
            capabilities,
            formats,
            present_modes,
        }
    }

    fn capabilities(&self) -> ash::vk::SurfaceCapabilitiesKHR {
        self.capabilities
    }

    fn formats(&self) -> &[ash::vk::SurfaceFormatKHR] {
        &self.formats
    }

    fn present_modes(&self) -> &[ash::vk::PresentModeKHR] {
        &self.present_modes
    }
}

pub struct HelloTriangleApplication {
    vulkan: Vulkan,
}

impl HelloTriangleApplication {
    fn new() -> Self {
        let sdl = Sdl::new();
        let vulkan = Vulkan::new(sdl, MAX_FRAMES_IN_FLIGHT);
        Self { vulkan }
    }
    fn run(&mut self) {
        self.main_loop();
    }
    fn main_loop(&mut self) {
        let mut event_pump = self.vulkan.sdl().sdl_context.event_pump().unwrap();
        'running: loop {
            for event in event_pump.poll_iter() {
                match event {
                    sdl2::event::Event::Quit { .. } => {
                        break 'running;
                    }
                    sdl2::event::Event::Window { win_event, .. } => {
                        if let sdl2::event::WindowEvent::Resized(..) = win_event {
                            self.vulkan.resize_happened = true;
                        }
                    }
                    _ => {}
                }
            }
            self.vulkan.draw_frame()
        }
        self.vulkan.wait_idle()
    }
}

fn main() {
    env_logger::init();
    let mut app = HelloTriangleApplication::new();
    app.run();
}

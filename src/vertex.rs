#[repr(C)]
#[derive(Debug)]
pub struct Vertex {
    pos: glm::Vec2,
    colour: glm::Vec3,
}

impl Vertex {
    pub fn new(pos: glm::Vec2, colour: glm::Vec3) -> Self {
        Self { pos, colour }
    }

    pub fn binding_description() -> ash::vk::VertexInputBindingDescription {
        let binding_description = ash::vk::VertexInputBindingDescription::default()
            .binding(0)
            .stride(core::mem::size_of::<Vertex>() as u32)
            .input_rate(ash::vk::VertexInputRate::VERTEX);
        binding_description
    }

    pub fn attribute_descriptions() -> [ash::vk::VertexInputAttributeDescription; 2] {
        [
            ash::vk::VertexInputAttributeDescription::default()
                .binding(0)
                .location(0)
                .format(ash::vk::Format::R32G32_SFLOAT)
                .offset(std::mem::offset_of!(Vertex, pos) as u32),
            ash::vk::VertexInputAttributeDescription::default()
                .binding(0)
                .location(1)
                .format(ash::vk::Format::R32G32B32_SFLOAT)
                .offset(std::mem::offset_of!(Vertex, colour) as u32)
        ]
    }
}

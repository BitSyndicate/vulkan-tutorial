use core::ffi::CStr;
use std::ffi::{c_char, c_void};

use sdl2::libc::strcmp;

#[cfg(debug_assertions)]
pub const ENABLE_VALIDATION_LAYERS: bool = true;

#[cfg(not(debug_assertions))]
pub const ENABLE_VALIDATION_LAYERS: bool = false;

pub const VALIDATION_LAYERS: [&CStr; 1] = [c"VK_LAYER_KHRONOS_validation"];
pub const VALIDATION_LAYERS_CPTR: [*const c_char; VALIDATION_LAYERS.len()] = {
    let mut idx = 0;
    let mut arr = [core::ptr::null(); VALIDATION_LAYERS.len()];
    while idx < VALIDATION_LAYERS.len() {
        arr[idx] = VALIDATION_LAYERS[idx].as_ptr();
        idx += 1;
    }
    arr
};

pub fn new_debug_messenger_create_info() -> ash::vk::DebugUtilsMessengerCreateInfoEXT<'static> {
    ash::vk::DebugUtilsMessengerCreateInfoEXT::default()
        .message_severity(
            ash::vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE
                | ash::vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
                | ash::vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
                | ash::vk::DebugUtilsMessageSeverityFlagsEXT::INFO,
        )
        .message_type(
            ash::vk::DebugUtilsMessageTypeFlagsEXT::DEVICE_ADDRESS_BINDING
                | ash::vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE
                | ash::vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                | ash::vk::DebugUtilsMessageTypeFlagsEXT::GENERAL,
        )
        .pfn_user_callback(Some(debug_callback))
}

pub fn check_validation_layer_support(entry: &ash::Entry) -> bool {
    let available_layers = unsafe { entry.enumerate_instance_layer_properties().unwrap() };

    for layer_name in VALIDATION_LAYERS {
        let mut layer_found = false;

        for layer_properties in available_layers.iter() {
            if layer_name
                .cmp(unsafe { CStr::from_ptr(layer_properties.layer_name.as_ptr()) })
                .is_eq()
            {
                layer_found = true;
                break;
            }
        }
        if !layer_found {
            return false;
        }
    }
    true
}

pub fn get_required_extensions(
    window: &sdl2::video::Window,
    entry: &ash::Entry,
) -> Vec<*const c_char> {
    // THIS, WITH A HIGH CHANCE, WILL BE A CAUSE OF BUGS IN THE FUTURE, BUT NOW IT'S OK
    let mut instance_extensions = window
        .vulkan_instance_extensions()
        .unwrap()
        .into_iter()
        .map(|extension| extension.as_ptr() as *const c_char)
        .collect::<Vec<_>>();
    let extensions = unsafe { entry.enumerate_instance_extension_properties(None).unwrap() };
    for extension in instance_extensions.iter() {
        let mut found = false;
        for ext in extensions.iter() {
            if unsafe { strcmp(ext.extension_name.as_ptr(), *extension) } == 0 {
                found = true;
                break;
            }
        }
        if !found {
            panic!(
                "Did not find vk_extension `{}` which is required",
                unsafe { CStr::from_ptr(*extension) }.to_str().unwrap()
            )
        }
    }

    if ENABLE_VALIDATION_LAYERS {
        instance_extensions.push(ash::ext::debug_utils::NAME.as_ptr())
    }
    instance_extensions
}

pub extern "system" fn debug_callback(
    message_severity: ash::vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: ash::vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const ash::vk::DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> ash::vk::Bool32 {
    let level = match message_severity {
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::INFO => log::Level::Info,
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => log::Level::Warn,
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => log::Level::Error,
        ash::vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE => log::Level::Debug,
        _ => log::Level::Trace,
    };
    let target = match message_type {
        ash::vk::DebugUtilsMessageTypeFlagsEXT::GENERAL => "Vulkan: General",
        ash::vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION => "Vulkan: Validation",
        ash::vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE => "Vulkan: Performance",
        ash::vk::DebugUtilsMessageTypeFlagsEXT::DEVICE_ADDRESS_BINDING => {
            "Vulkan: Device Address Binding"
        }
        _ => "Vulkan: Unknown",
    };

    log::log!(target: &target, level, "{}", unsafe {CStr::from_ptr((*p_callback_data).p_message) }.to_str().unwrap());
    ash::vk::FALSE
}

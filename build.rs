use std::{env, process::Command};

fn main() {
    println!("cargo::rerun-if-changed=shaders");
    let outdir = env::var("OUT_DIR").unwrap();
    let vert = Command::new("glslc")
        .args(["shaders/shader.vert", "-o", &format!("{outdir}/vert.spv")])
        .output()
        .expect("Failed to execute 'glslc'");
    let frag = Command::new("glslc")
        .args(["shaders/shader.frag", "-o", &format!("{outdir}/frag.spv")])
        .output()
        .expect("Failed to execute 'glslc'");
    if !vert.status.success() {
        panic!(
            "Failed to compile vertex shader: {}",
            String::from_utf8(vert.stderr).unwrap()
        )
    }
    if !frag.status.success() {
        panic!(
            "Failed to compile fragment shader: {}",
            String::from_utf8(frag.stderr).unwrap()
        )
    }
}
